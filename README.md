# Project Audiometry Service

a nodeJS service to support audiometry application

# audiometry

## Node JS setup
```
Download and Install Node JS from this link if you never install nodejs

https://nodejs.org/dist/v14.15.4/node-v14.15.4-x64.msi
```

## Git clone or download
```
You will need to git clone this repository
or
You can download the zip files https://gitlab.com/audiometry/audiometry-api/-/archive/master/audiometry-api-master.zip
```

## Text Editor
```
Open this FOLDER in text editor (I recommend VSCode)

Open terminal in vs code
```


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
