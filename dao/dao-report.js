const { db, pgp } = require('../utils/postgres')

class Client {
  constructor (userId = '') {
    this.userId = userId
  }

  async manageReportQuery (params) {
    const { company_id, type, date } = params
    let sql = null
    let dateSQL = ''

    // if (date) dateSQL = ` lm_dt BETWEEN SYMMETRIC '${ date[0] }' AND '${ date[1] }'`

    switch (type) {
      case 'all_employee':
        sql =
          `select a.name, a.nric, a.birth_dt, a.gender, a.lifestyle,
          a.employment_dt,date_part('year', age(birth_dt)) as age
          from adm_v2.tbl_employee a
          join adm_v2.tbl_user_company_employee b
          on a.uuid = b.employee_id
          where b.company_id = $[company_id]
          ${dateSQL}`

          try {
            const res = await db.any(sql, { company_id })
              .then(res => {
                return res.map(el => {
                  return {
                    ...el,
                    drug_affect_ear: el.lifestyle.drug_affect_ear ? 'Yes' : 'No',
                    hobby_affect_ear: el.lifestyle.hobby_affect_ear ? 'Yes' : 'No',
                    noisy_employment_hist: el.lifestyle.noisy_employment_hist ? 'Yes' : 'No',
                    past_ear_problem: el.lifestyle.past_ear_problem ? 'Yes' : 'No',
                    birth_dt: new Date(el.birth_dt).toLocaleDateString(),
                    employment_dt: new Date(el.employment_dt).toLocaleDateString()
                  }
                })
              })
      
            return res
          } catch (error) {
            console.log('all employee', error)
          }
        break;
      case 'annual':
        sql =
          `select b.name, b.nric, b.staff_id, a.right_ear, a.left_ear, a.exam_type, a.exam_dt
          from adm_v2.tbl_examination a
          join adm_v2.tbl_employee b
          on a.staff_id = b.staff_id
          join adm_v2.tbl_company c
          on a.company_id = c.uuid
          where a.company_id = $[company_id]
          and a.exam_type = 'Annual'
          ${dateSQL}`

          try {
            const res = await db.any(sql, { company_id })
              .then(res => {
                return res.map(el => {
                  return {
                    ...el,
                    r_500: el.right_ear['500Hz'],
                    r_1000: el.right_ear['1000Hz'],
                    r_2000: el.right_ear['2000Hz'],
                    r_3000: el.right_ear['3000Hz'],
                    r_4000: el.right_ear['4000Hz'],
                    r_6000: el.right_ear['6000Hz'],
                    r_8000: el.right_ear['8000Hz'],
                    l_500: el.left_ear['500Hz'],
                    l_1000: el.left_ear['1000Hz'],
                    l_2000: el.left_ear['2000Hz'],
                    l_3000: el.left_ear['3000Hz'],
                    l_4000: el.left_ear['4000Hz'],
                    l_6000: el.left_ear['6000Hz'],
                    l_8000: el.left_ear['8000Hz'],
                    exam_dt: new Date(el.exam_dt).toLocaleDateString()
                  }
                })
              })

            return res
          } catch (error) {
            console.log('annual', error)
          }
        break;
      case 'baseline':
        sql =
          `select b.name, b.nric, b.staff_id, a.right_ear, a.left_ear, a.exam_type, a.exam_dt
          from adm_v2.tbl_examination a
          join adm_v2.tbl_employee b
          on a.staff_id = b.staff_id
          join adm_v2.tbl_company c
          on a.company_id = c.uuid
          where a.company_id = $[company_id]
          and a.exam_type = 'Baseline'
          ${dateSQL}`

        try {
          const res = await db.any(sql, { company_id })
            .then(res => {
              return res.map(el => {
                return {
                  ...el,
                  r_500: el.right_ear['500Hz'],
                  r_1000: el.right_ear['1000Hz'],
                  r_2000: el.right_ear['2000Hz'],
                  r_3000: el.right_ear['3000Hz'],
                  r_4000: el.right_ear['4000Hz'],
                  r_6000: el.right_ear['6000Hz'],
                  r_8000: el.right_ear['8000Hz'],
                  l_500: el.left_ear['500Hz'],
                  l_1000: el.left_ear['1000Hz'],
                  l_2000: el.left_ear['2000Hz'],
                  l_3000: el.left_ear['3000Hz'],
                  l_4000: el.left_ear['4000Hz'],
                  l_6000: el.left_ear['6000Hz'],
                  l_8000: el.left_ear['8000Hz'],
                  exam_dt: new Date(el.exam_dt).toLocaleDateString()
                }
              })
            })

          return res
        } catch (error) {
          console.log('baseline', error)
        }
        break;
      case 'sts':
        sql =
          `select b.name, b.nric, b.staff_id, a.exam_type, a.sts, a.exam_dt
          from adm_v2.tbl_examination a
          join adm_v2.tbl_employee b
          on a.staff_id = b.staff_id
          join adm_v2.tbl_company c
          on a.company_id = c.uuid
          where a.company_id = $[company_id]
          and (a.sts->>'left_ear')::boolean is true
          or (a.sts->>'right_ear')::boolean is true
          ${dateSQL}`

        try {
          const res = await db.any(sql, { company_id })
            .then(res => {
              return res.map(el => {
                return {
                  ...el,
                  sts_right: el.sts.right_ear ? 'Yes' : 'No',
                  sts_left: el.sts.left_ear ? 'Yes' : 'No',
                  exam_dt: new Date(el.exam_dt).toLocaleDateString()
                }
              })
            })

          return res
        } catch (error) {
          console.log('sts', error)
        }
        break;
      case 'hi':
        sql =
          `select b.name, b.nric, b.staff_id, a.exam_type, a.hi, a.exam_dt
          from adm_v2.tbl_examination a
          join adm_v2.tbl_employee b
          on a.staff_id = b.staff_id
          join adm_v2.tbl_company c
          on a.company_id = c.uuid
          where a.company_id = $[company_id]
          and (a.hi->>'left_ear')::boolean is true
          or (a.hi->>'right_ear')::boolean is true
          ${dateSQL}`

        try {
          const res = await db.any(sql, { company_id })
            .then(res => {
              return res.map(el => {
                return {
                  ...el,
                  hi_right: el.hi.right_ear ? 'Yes' : 'No',
                  hi_left: el.hi.left_ear ? 'Yes' : 'No',
                  exam_dt: new Date(el.exam_dt).toLocaleDateString()
                }
              })
            })

          return res
        } catch (error) {
          console.log('hearing_impairment', error)
        }
        break;
      case 'nihl':
        sql =
          `select b.name, b.nric, b.staff_id, a.exam_type, a.nihl, a.exam_dt
          from adm_v2.tbl_examination a
          join adm_v2.tbl_employee b
          on a.staff_id = b.staff_id
          join adm_v2.tbl_company c
          on a.company_id = c.uuid
          where a.company_id = $[company_id]
          and (a.nihl->>'left_ear')::boolean is true
          or (a.nihl->>'right_ear')::boolean is true
          ${dateSQL}`

        try {
          const res = await db.any(sql, { company_id })
            .then(res => {
              return res.map(el => {
                return {
                  ...el,
                  nihl_right: el.nihl.right_ear ? 'Yes' : 'No',
                  nihl_left: el.nihl.left_ear ? 'Yes' : 'No',
                  exam_dt: new Date(el.exam_dt).toLocaleDateString()
                }
              })
            })

          return res
        } catch (error) {
          console.log('nihl', error)
        }
        break;
      case 'required_exam':
        break;
      case 'no_test':
        break;
      default:
        break;
    }
      console.log('🚀 > file: dao-report.js > line 200 > Client > manageReportQuery > sts', sts)
  }
}

module.exports = new Client