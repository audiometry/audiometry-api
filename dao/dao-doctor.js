const { db, pgp } = require('../utils/postgres')

class Client {
  constructor (userId = '') {
    this.userId = userId
  }

  async checkDoctorEntry (params) {
    const sql =
      `select exists (
        select uuid from tbl_user_doctor
        where user_id = $[user_id] and doctor_id = $[nric]
       )`

    const res = await db.any(sql, params)
    
    return res
  }

  async fetchAllDoctor () {
    const sql =
    `select uuid, name, nric,
     phone, email
     from tbl_doctor`

    const res = await db.any(sql)
    
    return res
  }

  async fetchByUser (params) {
    const sql =
      `select b.uuid, name, nric, phone, email, lm_dt
      from adm_v2.tbl_user_doctor a
      join adm_v2.tbl_doctor b
      on a.doctor_id = b.nric
      where a.user_id = $[user_id]`

    const res = await db.any(sql, params)
      .then(res => {
        return res.map(el => {
          return { ...el, lm_dt: new Date(el.lm_dt).toLocaleDateString() }
        })
      })
    
    return res
  }

  async manageDoctor (payload) {
    const { uuid, name } = payload

    /**
     * if uuid is not present its insert since uuid is pg generated
     * if uuid is present but name is not, its update else its delete
     */

    if (!uuid) {
      // insert
      try {
        const { user_id, ...rest } = payload
        db.task(async t => {
          const a = await t.one(`insert into tbl_doctor($1~) values($1:list) returning nric`, [rest])
          const b = await t.none(`insert into tbl_user_doctor($1~) values($1:list)`, [{ user_id: user_id, doctor_id: a.nric }])
       })
       .catch(error => {
          console.log('🚀 ~ manageDoctor > insert > task > error', error)
       })
      } catch (error) {
        console.log("🚀 ~ manageDoctor > insert > error", error)
      }
    } else {
      if (name) {
        // update
        try {
          const sql =
          `update tbl_doctor
           set name = $[name], email = $[email],
           phone = $[phone], lm_dt = now()
           where uuid = $[uuid]`
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log("🚀 ~ manageDoctor > update > error", error)
        }
      } else {
        // delete
        try {
          const sql = 'delete from tbl_user_doctor where doctor_id = $[uuid] and user_id = $[user_id]'
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log("🚀 ~ manageDoctor > delete > error", error)
        }
      }
    }
  }
}

module.exports = new Client