const { db, pgp } = require('../utils/postgres')
const { pickObject } = require('../utils/common')


class Employee {
  constructor (userId = '') {
    this.userId = userId
  }

  async fetchEmployeeByFactory (params) {
    const sql =
    `select emp.emp_id, emp.emp_name, emp.emp_dob, emp.emp_sex, empl.empl_staff_id,
     empl.empl_date, empl.empl_role, empl.empl_dept, empl.empl_sect, empl.empl_question
     from adm_dev.tbl_employee emp
     join adm_dev.tbl_employment empl on empl.empl_emp_id = emp.emp_id
     join adm_dev.tbl_client client on client.client_id = empl.empl_client_id
     where client.client_id = $[client_id]`

    const res = await db.any(sql, params)

    return res
  }

  async fetchEmployeeAll () {
    // const sql =
    // `select emp.emp_ic_num, empl.empl_emp_id, emp.emp_name,
    //  adm.adm_type,empl.empl_job, empl.empl_dept, empl.empl_sect
    //  from adm_dev.tbl_employee emp
    //  join adm_dev.tbl_employment_hist empl on empl.empl_emp_ic = emp.emp_ic_num
    //  join adm_dev.tbl_audiometric_result adm on empl.empl_emp_id = adm.adm_emp_id`

    const sql =
    `select emp.emp_ic_num, empl.empl_emp_id, emp.emp_name,
     empl.empl_job, empl.empl_dept, empl.empl_sect
     from adm_dev.tbl_employee emp
     join adm_dev.tbl_employment_hist empl on empl.empl_emp_ic = emp.emp_ic_num`

    const res = await db.any(sql)

    return res
  }

  async fetchEmployee (params) {
    const sql =
    `select * from (
      select emp.emp_id, empl.empl_emp_ic, empl.empl_emp_id, emp.emp_name, emp.emp_dob, fac.fac_name, emp.emp_sex,
      empl.empl_job, empl.empl_dept, empl.empl_sect, empl.empl_precond_id, empl.empl_date, empl.empl_baseline_amnd
      from adm_dev.tbl_employee emp
      join adm_dev.tbl_employment_hist empl on emp.emp_ic_num = empl.empl_emp_ic
      join adm_dev.tbl_factory fac on empl.empl_fac_id=fac.fac_id
      where emp.emp_ic_num = $[emp_ic_num]
    ) empl
    join adm_dev.tbl_precondition cond on empl.empl_precond_id = cond.cond_id`

    const res = await db.any(sql, params)

    return res
  }

  async manageEmployee (payload) {
    const { isNew } = payload

    if (isNew) {
      await createEmployee ()
    } else {
      await updateEmployee ()
    }
  }

  async createEmployee (payload) {
    const queries = {}

    // add to tbl_employee
    queries.employee = {
      sql: `insert into tbl_employee($1~) values($1:list)`,
      params: pickObject( payload, ['emp_id', 'emp_name', 'emp_dob', 'emp_sex'])
    }

    // add to tbl_employment_hist
    queries.employment = {
      sql: `insert into tbl_employment($1~) values($1:list)`,
      params: 
      {
          ...pickObject(payload, ['empl_staff_id', 'empl_role', 'empl_dept', 'empl_sect', 'empl_date', 'empl_question']),
          empl_client_id: payload.client_id,
          empl_emp_id: payload.emp_id
        }
    }

    return db.tx(t => {
      return t.batch([
          t.none(queries.employee.sql, [queries.employee.params]),
          t.none(queries.employment.sql, [queries.employment.params]),
      ])
    })
    .catch(error => {
      console.log('Creating new employee error', error)
    })
  }

  async updateEmployee (payload) {}

  // new employee with adm test result
  async manageEmployeeADM (payload) {
    console.log('TCL: Employee -> manageEmployeeADM -> payload', payload)
    const queries = {}

    // add to tbl_employee
    queries.employee = {
      sql: `insert into tbl_employee($1~) values($1:list) returning emp_id`,
      params: {
        emp_ic_num: payload.emp.empl_emp_ic,
        ...pickObject( payload.emp, ['emp_name', 'emp_dob', 'emp_sex'])
      }
    }
    // add to tbl_precond
    queries.precondition = {
      sql: `insert into tbl_precondition($1~) values($1:list) returning cond_id`,
      params: pickObject(payload.emp, ['cond_past_prob', 'cond_hobby_ear', 'cond_drug_ear', 'cond_empl_noisy'])
    }
    // add to tbl_employment_hist
    queries.employment = {
      sql: `insert into tbl_employment_hist($1~) values($1:list) returning empl_id`,
      params: 
      {
          ...pickObject(payload.emp, ['empl_fac_id', 'empl_job', 'empl_dept', 'empl_sect', 'empl_date', 'empl_baseline_amnd']),
          empl_emp_ic: payload.emp.empl_emp_ic,
          empl_emp_id: payload.emp.empl_emp_id
        }
    }
    // add to tbl_audiometric_result
    queries.audiometric = {
      sql: `insert into tbl_audiometric_result($1~) values($1:list) returning adm_id`,
      params: {
        ...pickObject(payload.adm, ['adm_exposure', 'adm_max', 'adm_peak', 'adm_type', 'adm_date']),
        adm_emp_id: payload.emp.empl_emp_id,
        adm_r_ear: JSON.stringify({ ...pickObject(payload.adm, ['adm_r_ear_500hz', 'adm_r_ear_1000hz', 'adm_r_ear_2000hz', 'adm_r_ear_3000hz', 'adm_r_ear_4000hz', 'adm_r_ear_6000hz', 'adm_r_ear_8000hz']) }),
        adm_l_ear: JSON.stringify({ ...pickObject(payload.adm, ['adm_l_ear_500hz', 'adm_l_ear_1000hz', 'adm_l_ear_2000hz', 'adm_l_ear_3000hz', 'adm_l_ear_4000hz', 'adm_l_ear_6000hz', 'adm_l_ear_8000hz']) })
       }
    }

    db.tx(t => {
      return t.batch([
          t.one(queries.employee.sql, [queries.employee.params]),
          t.one(queries.precondition.sql, [queries.precondition.params]),
      ]);
    })
    .then(data => {
      db.tx (t => {
        return t.batch([
          t.one(queries.employment.sql, [{ ...queries.employment.params, empl_precond_id: data[1].cond_id }]),
          t.one(queries.audiometric.sql, [queries.audiometric.params])
        ])
      })
    })
    .catch(error => {
      console.log('Creating new employee error', error)
    })
  }

  // new employee data only
  async manageEmployees (payload) {
    const queries = {}

    if (payload.isNew) {
      // add to tbl_employee
      queries.employee = {
        sql: `insert into tbl_employee($1~) values($1:list)`,
        params: pickObject( payload, ['emp_id', 'emp_name', 'emp_dob', 'emp_sex'])
      }

      // add to tbl_employment_hist
      queries.employment = {
        sql: `insert into tbl_employment($1~) values($1:list)`,
        params: 
        {
            ...pickObject(payload, ['empl_staff_id', 'empl_role', 'empl_dept', 'empl_sect', 'empl_date', 'empl_question']),
            empl_client_id: payload.client_id,
            empl_emp_id: payload.emp_id
          }
      }

      return db.tx(t => {
        return t.batch([
            t.none(queries.employee.sql, [queries.employee.params]),
            t.none(queries.employment.sql, [queries.employment.params]),
        ])
      })
      .catch(error => {
        console.log('Creating new employee error', error)
      })
    }
    // else {
    //   // update
    //   // update tbl_employee
    //   queries.employee = {
    //     sql:
    //     `update tbl_employee
    //      set emp_name = $[emp_name], emp_dob = $[emp_dob],
    //      emp_sex = $[emp_sex], lm_dt = now()
    //      where emp_ic_num = $[empl_emp_ic]`,
    //     params: pickObject( payload, ['emp_name', 'emp_dob', 'emp_sex', 'empl_emp_ic'])
    //   },

    //   // update tbl_precond
    //   queries.precondition = {
    //     sql:
    //     `update tbl_precondition
    //      set cond_past_prob = $[cond_past_prob], cond_hobby_ear = $[cond_hobby_ear],
    //      cond_drug_ear = $[cond_drug_ear], cond_empl_noisy = $[cond_empl_noisy],
    //      lm_dt = now()
    //      where cond_id = $[cond_id]`,
    //     params: pickObject(payload, ['cond_past_prob', 'cond_hobby_ear', 'cond_drug_ear', 'cond_empl_noisy', 'cond_id'])
    //   }
    // }

    // // update tbl_employment_hist
    // queries.employment = {
    //   sql:
    //   `update tbl_employment_hist
    //    set empl_date = $[empl_date], empl_job = $[empl_job],
    //    empl_dept = $[empl_dept], empl_sect = $[empl_sect],
    //    empl_baseline_amnd = $[empl_baseline_amnd], lm_dt = now()
    //    where empl_emp_ic = $[empl_emp_ic]`,
    //   params: pickObject(payload, ['empl_date', 'empl_job', 'empl_dept', 'empl_sect', 'empl_date', 'empl_baseline_amnd', 'empl_emp_ic'])
    // }

    // return db.tx(t => {
    //   return t.batch([
    //       t.none(queries.employee.sql, queries.employee.params),
    //       t.none(queries.precondition.sql, queries.precondition.params),
    //       t.none(queries.employment.sql, queries.employment.params)
    //   ]);
    // })
    // .catch(error => {
    //   console.log('Updating employee error', error)
    // })
  }

  async manageEmployeeDummy () {
    const collection = []
    const queriesCollect = []

    for (let i = 0; i < 100; i++) {
      const payload = {
        adm: {
          adm_date: '2019-12-08',
          adm_exposure: `5${i}`,
          adm_l_ear_500hz: `2${i}`,
          adm_l_ear_1000hz: `2${i}`,
          adm_l_ear_2000hz: `2${i}`,
          adm_l_ear_3000hz: `2${i}`,
          adm_l_ear_4000hz: `2${i}`,
          adm_l_ear_6000hz: `2${i}`,
          adm_l_ear_8000hz: `2${i}`,
          adm_max: '110',
          adm_peak: '140',
          adm_r_ear_500hz: `2${i}`,
          adm_r_ear_1000hz: `2${i}`,
          adm_r_ear_2000hz: `2${i}`,
          adm_r_ear_3000hz: `2${i}`,
          adm_r_ear_4000hz: `2${i}`,
          adm_r_ear_6000hz: `2${i}`,
          adm_r_ear_8000hz: `2${i}`,
          adm_type: 'Baseline Audiogram'
        }, 
        emp: {
          cond_drug_ear: true,
          cond_empl_noisy: true,
          cond_hobby_ear: true,
          cond_past_prob: true,
          emp_dob: '1965-01-01',
          emp_ic_num: `0112233${i}`,
          emp_name: `Employee-${i}`,
          emp_sex: 'Male',
          empl_baseline_amnd: true,
          empl_date: '2019-02-02',
          empl_dept: `dept-${i}`,
          empl_emp_id: `4556677${i}`,
          empl_fac_id: 17 + i,
          empl_job: `job-${i}`,
          empl_sect: `sect-${i}`,
          fac_name: `factory-${i}`,
        }
      }
      collection.push(payload)
    }

    collection.forEach(el => {
      const queries = {}

      // add to tbl_employee
      queries.employee = {
        sql: `insert into tbl_employee($1~) values($1:list) returning emp_id`,
        params: pickObject( el.emp, ['emp_ic_num', 'emp_name', 'emp_dob', 'emp_sex'])
      }
      // add to tbl_precond
      queries.precondition = {
        sql: `insert into tbl_precondition($1~) values($1:list) returning cond_id`,
        params: pickObject(el.emp, ['cond_past_prob', 'cond_hobby_ear', 'cond_drug_ear', 'cond_empl_noisy'])
      }
      // add to tbl_employment_hist
      queries.employment = {
        sql: `insert into tbl_employment_hist($1~) values($1:list) returning empl_id`,
        params: 
        {
            ...pickObject(el.emp, ['empl_fac_id', 'empl_job', 'empl_dept', 'empl_sect', 'empl_date', 'empl_baseline_amnd']),
            empl_emp_ic: el.emp.emp_ic_num,
            empl_emp_id: el.emp.empl_emp_id
          }
      }
      // add to tbl_audiometric_result
      queries.audiometric = {
        sql: `insert into tbl_audiometric_result($1~) values($1:list) returning adm_id`,
        params: {
          ...pickObject(el.adm, ['adm_exposure', 'adm_max', 'adm_peak', 'adm_type', 'adm_date']),
          adm_emp_id: el.emp.empl_emp_id,
          adm_r_ear: JSON.stringify({ ...pickObject(el.adm, ['adm_r_ear_500hz', 'adm_r_ear_1000hz', 'adm_r_ear_2000hz', 'adm_r_ear_3000hz', 'adm_r_ear_4000hz', 'adm_r_ear_6000hz', 'adm_r_ear_8000hz']) }),
          adm_l_ear: JSON.stringify({ ...pickObject(el.adm, ['adm_l_ear_500hz', 'adm_l_ear_1000hz', 'adm_l_ear_2000hz', 'adm_l_ear_3000hz', 'adm_l_ear_4000hz', 'adm_l_ear_6000hz', 'adm_l_ear_8000hz']) })
        }
      }

      queriesCollect.push(queries)
    })

    queriesCollect.forEach(async query => {
      await db.tx(t => {
        return t.batch([
            t.one(query.employee.sql, [query.employee.params]),
            t.one(query.precondition.sql, [query.precondition.params]),
        ]);
      })
      .then(data => {
        db.tx (t => {
          return t.batch([
            t.one(query.employment.sql, [{ ...query.employment.params, empl_precond_id: data[1].cond_id }]),
            t.one(query.audiometric.sql, [query.audiometric.params])
          ])
        })
      })
      .catch(error => {
        console.log('Creating new employee error', error)
      })
    })
  }

  async fetchReportEmployeeAll () {
    const sql =
    `select emp.emp_name, emp.emp_ic_num, adm.adm_l_ear, adm.adm_r_ear
     from adm_dev.tbl_employee emp
     join adm_dev.tbl_employment_hist empl on emp.emp_ic_num = empl.empl_emp_ic
     join adm_dev.tbl_audiometric_result adm on empl.empl_emp_id = adm.adm_emp_id
     limit 30`

    const res = await db.any(sql)

    const mod = res.map(el => {
      return {
        ...el,
        ...JSON.parse(el.adm_r_ear),
        ...JSON.parse(el.adm_l_ear)
      }
    })

    return mod
  }
}

module.exports = new Employee