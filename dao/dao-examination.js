const { db, pgp } = require('../utils/postgres')

class Client {
  constructor (userId = '') {
    this.userId = userId
  }

  async checkExamType (params) {
    const sql =
    `select distinct exam_type
    from adm_v2.tbl_examination
    where staff_id = $[staff_id]`

    const res = await db.any(sql, params)

    if (res.length !== 0) {
      const type = res.map(el => el.exam_type)

      if (type.includes('Baseline')) return 'Annual'
    } else return 'Baseline'
  }

  async fetchCompanyID (params) {
    const sql =
    `select a.company_id
    from adm_v2.tbl_user_company_employee a
    join adm_v2.tbl_employee b
    on a.employee_id = b.uuid
    where b.staff_id = $[staff_id]`

    const res = await db.any(sql, params)

    return res
  }

  async fetchByUser (params) {
    const sql =
      `select a.uuid, a.staff_id, a.doctor_id, f.name as doctor_name,
      a.audiometer_id, g.name as audiometer_name, g.model, g.serial, g.calib_dt,
      a.exposure, a.peak_exposure, a.max_exposure,
      a.right_ear, a.left_ear, a.nihl, a.sts, a.hi, a.remark, a.exam_type, a.exam_dt, a.lm_dt,
      e.name as user_name, b.user_id, d.name as company,
      c.name as staff_name, c.nric, c.birth_dt, c.gender, c.job, c.section, c.department, c.employment_dt, c.lifestyle  
      from adm_v2.tbl_examination a
      join adm_v2.tbl_user_examination b on a.uuid = b.examination_id
      join adm_v2.tbl_employee c on a.staff_id = c.staff_id
      join adm_v2.tbl_company d on a.company_id = d.uuid
      join adm_v2.tbl_user e on b.user_id = e.uuid
      join adm_v2.tbl_doctor f on a.doctor_id = f.uuid
      join adm_v2.tbl_audiometer g on a.audiometer_id = g.uuid
      where b.user_id = $[user_id]`

    const res = await db.any(sql, params)
      .then(res => {
        return res.map(el => {
          return {
            ...el,
            birth_dt: new Date(el.birth_dt).toLocaleDateString(),
            calib_dt: new Date(el.calib_dt).toLocaleDateString(),
            employment_dt: new Date(el.employment_dt).toLocaleDateString(),
            exam_dt: new Date(el.exam_dt).toLocaleDateString(),
            lm_dt: new Date(el.lm_dt).toLocaleDateString()
          }
        })
      })

    return res
  }

  async manageExamination (payload) {
    const { uuid, name } = payload

    if (uuid === undefined) {
      // insert
      try {
        const { user_id, ...rest } = payload
        db.task(async t => {
          const a = await t.one(`insert into tbl_examination($1~) values($1:list) returning uuid`, [rest])

          const b = await t.none(`insert into tbl_user_examination($1~) values($1:list)
            on conflict (user_id, examination_id) do nothing`, [{ user_id, examination_id: a.uuid }])
       })
       .catch(error => {
          console.log('🚀 ~ manageExamination > insert > task > error', error)
       })
      } catch (error) {
        console.log("🚀 ~ manageExamination > insert > error", error)
      }
    } else {
      // update || delete

      // i dont send anything except uuid, so we will know, its delete
      if (name !== undefined) {
        //update row
        try {
          const sql =
          `update tbl_examination
           set doctor_id = $[doctor_id], audiometer_id = $[audiometer_id],
           exposure = $[exposure], max_exposure = $[max_exposure], peak_exposure = $[peak_exposure],
           right_ear = $[right_ear], left_ear = $[left_ear],  sts = $[sts], nihl = $[nihl], hi = $[hi],
           lm_dt = now()  where uuid = $[uuid]`
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log('🚀 > manageExamination > update > error', error)
        }
      } else {
        // delete row
        try {
          const sql =
          `delete from tbl_user_examination
          where user_id = $[user_id]
          and examination_id = $[uuid]`
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log('🚀 > manageExamination > delete > error', error)
        }
      }
    }
  }

  async fetchExaminationReport (params) {
    const sql =
    `select a.audiogram_type, a.exposure, a.max_exposure, a.peak_exposure, a.l_ear, a.r_ear, a.sts, a.nihl, a.hearing_impairment, a.remark, a.test_date,
      b.name as employee_name, b.nric, b.birth_date, b.gender, b.job, b.employee_num, b.employment_date, b.department, b.section,
      c.name as audiometer, c.serial_num, c.model_num, c.calib_date,
      d.name as review_by,
      e.name as employer
      from adm_dev.tbl_examination a
      join adm_dev.tbl_employee b on a.employee_uuid = b.uuid
      join adm_dev.tbl_equipment c on a.audiometer = c.uuid
      join adm_dev.tbl_doctor d on a.review_by = d.uuid
      join adm_dev.tbl_company e on a.current_employment = e.uuid
      where a.uuid = $[uuid]`

    const res = await db.any(sql, params)

    return res
  }
}

module.exports = new Client