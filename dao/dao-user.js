const { db, pgp } = require('../utils/postgres')

class Client {
  constructor (userId = '') {
    this.userId = userId
  }

  async createUser (payload) {
    try {
      const sql = `
        insert into tbl_user($1~) values($1:list)
        returning uuid, name, email, phone, address`
      const res = await db.one(sql, [payload])
      return res
    } catch (error) {
      return error
    }
  }

  async updateUser (payload) {
    try {
      const sql = `update tbl_user set name = $[name],
        email = $[email], phone = $[phone], address = $[address], lm_date = now()
        where uuid = $[uuid]`

      const res = await db.none(sql, payload)
      return res
    } catch (error) {
      return { code: 409, data: String(error.detail).replace('Key ', '') }
    }
  }

  async findUserById (uuid) {
    try {
      const sql = `select uuid, name, phone, email, address from tbl_user where uuid = $1`
      const res = await db.oneOrNone(sql, [uuid])
      return res
    } catch (error) {
      return { code: 409, data: String(error.detail) }
    }
  }

  async verifyUser (email) {
    try {
      const sql = `select * from tbl_user where email = $1`
      const res = await db.oneOrNone(sql, [email])
      return res
    } catch (error) {
      return error
    }
  }
}

module.exports = new Client