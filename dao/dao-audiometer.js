const { db, pgp } = require('../utils/postgres')

class Client {
  constructor (userId = '') {
    this.userId = userId
  }

  async checkAudiometerEntry (params) {
    const sql =
      `select exists (
        select b.uuid
        from tbl_audiometer a
        join tbl_user_audiometer b
        on a.uuid = b.audiometer_id
        where a.model = $[model] and a.serial = $[serial]
        and b.user_id = $[user_id]
       )`

    const res = await db.any(sql, params)
    
    return res
  }

  async fetchAllAudiometer() {
    const sql = 'select uuid, name, serial, model, calibration_dt, lm_dt from tbl_audiometer'

    const res = await db.any(sql, params)
      .then(res => {
        const [year, month, day] = el.calibration_dt.split('-')
        return res.map(el => {
          return {
            ...el,
            display_calib_dt: `${day}/${month}/${year}`,
            lm_dt: new Date(el.lm_dt).toLocaleDateString()
          }
        })
      })
    
    return res
  }

  async fetchByUser (params) {
    const sql =
      `select a.uuid, a.name, a.serial, a.model, a.calib_dt
      from tbl_audiometer a
      join tbl_user_audiometer b
      on a.uuid = b.audiometer_id
      where user_id = $[user_id]`

     const res = await db.any(sql, params)
   
    return res
  }

  async manageAudiometer (payload) {
    const { uuid, name } = payload

    /**
     * if uuid is not present its insert since uuid is pg generated
     * if uuid is present but name is not, its update else its delete
     */

    if (!uuid) {
      // insert
      try {
        const { user_id, ...rest } = payload
        db.task(async t => {
          const a = await t.one(`insert into tbl_audiometer($1~) values($1:list)
            on conflict (serial, model) do update set lm_dt = now() returning uuid`, [rest])

          const b = await t.none(`insert into tbl_user_audiometer($1~) values($1:list)`, [{ user_id: user_id, audiometer_id: a.uuid }])
       })
       .catch(error => {
          console.log('🚀 ~ manageAudiometer > insert > task > error', error)
       })
      } catch (error) {
        console.log("🚀 ~ manageAudiometer > insert > error", error)
      }
    } else {
      if (name) {
        // update
        try {
          const sql =
          `update tbl_audiometer
           set name = $[name], calib_dt = $[calib_dt], lm_dt = now()
           where uuid = $[uuid]`
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log("🚀 ~ manageAudiometer > update > error", error)
        }
      } else {
        // delete
        try {
          const sql = 'delete from tbl_user_audiometer where audiometer_id = $[uuid] and user_id = $[user_id]'
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log("🚀 ~ manageAudiometer > delete > error", error)
        }
      }
    }
  }
}

module.exports = new Client