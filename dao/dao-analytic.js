const { db, pgp } = require('../utils/postgres')

class Client {
  constructor (userId = '') {
    this.userId = userId
  }

  async fetchExamAnalytics (params) {
    const sql =
      `select c.name, c.nric, a.exam_dt, a.lm_dt
      from adm_v2.tbl_examination a
      join adm_v2.tbl_user_examination b
      on a.uuid = b.examination_id
      join adm_v2.tbl_employee c
      on a.staff_id = c.staff_id
      where b.user_id = $[user_id]
      and a.lm_dt > current_date - interval '60' day`

    const res = await db.any(sql, params)
    .then(res => {
      return res.map(el => {
        return {
          ...el,
          lm_dt: new Date(el.lm_dt).toLocaleDateString()
        }
      })
    })

    return res
  }
}

module.exports = new Client