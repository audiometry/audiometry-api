const { db, pgp } = require('../utils/postgres')

class Client {
  constructor (userId = '') {
    this.userId = userId
  }

  async fetchCompanyFilter (params) {
    const sql =
      `select distinct a.uuid, a.name, a.email, a.address
      from adm_v2.tbl_company a
      join adm_v2.tbl_user_company b
      on a.uuid = b.company_id
      where user_id = $[user_id]`

    const res = await db.any(sql, params)

    return res
  }

  async fetchJobFilter () {
    const sql = 'select distinct job from tbl_employee'

    const res = await db.any(sql)

    return res
  }

  async fetchDepartmentFilter (params) {
    const sql = 'select distinct department from tbl_employee'

    const res = await db.any(sql, params)

    return res
  }

  async fetchSectionFilter (params) {
    const sql = 'select distinct section from tbl_employee'

    const res = await db.any(sql, params)
    
    return res
  }

  async fetchDoctorFilter (params) {
    const sql = 'select uuid, name, email from tbl_doctor group by uuid, name, email'

    const res = await db.any(sql, params)
    
    return res
  }

  async fetchAudiometerFilter (params) {
    const sql = 'select uuid, name, serial, model from adm_v2.tbl_audiometer group by uuid, name, serial, model'

    const res = await db.any(sql, params)
    
    return res
  }
}

module.exports = new Client