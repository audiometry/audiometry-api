const { db, pgp } = require('../utils/postgres')

class Client {
  constructor (userId = '') {
    this.userId = userId
  }

  async checkEmployeeEntry (params) {
    const sql =
      `select exists (
        select b.uuid
        from tbl_employee a
        join tbl_user_company_employee b
        on a.uuid = b.employee_id
        where a.staff_id = $[staff_id]
        and b.user_id = $[user_id]
      )`

    const res = await db.any(sql, params)
    
    return res
  }

  async fetchByUser (params) {
    const sql =
    `select a.*, b.company_id, c.name as company
    from adm_v2.tbl_employee a
    join adm_v2.tbl_user_company_employee b
    on a.uuid = b.employee_id
    join adm_v2.tbl_company c
    on b.company_id = c.uuid
    where b.user_id = $[user_id]`

    const res = await db.any(sql, params)
      .then(res => {
        return res.map(el => {
          return {
            ...el,
            lm_dt: new Date(el.lm_dt).toLocaleDateString()
          }
        })
      })
    
    return res
  }

  async manageEmployee (payload) {
    const { uuid, name } = payload

    /**
     * if uuid is not present its insert since uuid is pg generated
     * if uuid is present but name is not, its update else its delete
     */

    if (!uuid) {
      // insert
      try {
        const { user_id, company_id, ...rest } = payload
        db.task(async t => {
          const a = await t.one(`insert into tbl_employee($1~) values($1:list)
            on conflict (staff_id) do update set lm_dt = now() returning uuid`, [rest])

          const b = await t.none(`insert into tbl_user_company_employee($1~) values($1:list)
            on conflict (user_id, employee_id, company_id) do nothing`, [{ user_id, company_id, employee_id: a.uuid }])
       })
       .catch(error => {
          console.log('🚀 ~ manageEmployee > insert > task > error', error)
       })
      } catch (error) {
        console.log("🚀 ~ manageEmployee > insert > error", error)
      }
    } else {
      if (name) {
        // update
        try {
          const sql =
          `update tbl_employee
          set name = $[name], gender = $[gender], birth_dt = $[birth_dt],
          job = $[job], section = $[section], department = $[department],
          employment_dt = $[employment_dt], lifestyle = $[lifestyle],
          lm_dt = now() where uuid = $[uuid]`
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log("🚀 ~ manageEmployee > update > error", error)
        }
      } else {
        // delete
        try {
          const sql =
          `delete from tbl_user_company_employee
          where employee_id = $[uuid]
          and user_id = $[user_id]
          and company_id = $[company_id]`
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log("🚀 ~ manageEmployee > delete > error", error)
        }
      }
    }
  }
}

module.exports = new Client