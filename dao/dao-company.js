const { db, pgp } = require('../utils/postgres')

class Client {
  constructor (userId = '') {
    this.userId = userId
  }

  async checkCompanyEntry (params) {
    const sql =
      `select exists (
        select b.uuid
        from tbl_company a
        join tbl_user_company b
        on a.uuid = b.company_id
        where a.email = $[email]
        and b.user_id = $[user_id]
       )`

    const res = await db.any(sql, params)
    
    return res
  }

  async fetchByUser (params) {
    const sql =
      `select a.uuid, a.name, a.email, a.phone, a.address, a.lm_dt
      from tbl_company a
      join tbl_user_company b
      on a.uuid = b.company_id
      where user_id = $[user_id]`

    const res = await db.any(sql, params)
      .then(res => {
        return res.map(el => {
          return {
            ...el,
            lm_dt: new Date(el.lm_dt).toLocaleDateString()
          }
        })
      })
    
    return res
  }

  async manageCompany (payload) {
    const { uuid, name } = payload

    /**
     * if uuid is not present its insert since uuid is pg generated
     * if uuid is present but name is not, its update else its delete
     */

    if (!uuid) {
      // insert
      try {
        const { user_id, ...rest } = payload
        db.task(async t => {
          const a = await t.one(`insert into tbl_company($1~) values($1:list)
            on conflict (email) do update set lm_dt = now() returning uuid`, [rest])

          const b = await t.none(`insert into tbl_user_company($1~) values($1:list)`, [{ user_id: user_id, company_id: a.uuid }])
       })
       .catch(error => {
          console.log('🚀 ~ manageCompany > insert > task > error', error)
       })
      } catch (error) {
        console.log("🚀 ~ manageCompany > insert > error", error)
      }
    } else {
      if (name) {
        // update
        try {
          const sql =
          `update tbl_company
           set name = $[name], phone = $[phone],
           address = $[address], lm_dt = now()
           where uuid = $[uuid]`
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log("🚀 ~ manageCompany > update > error", error)
        }
      } else {
        // delete
        try {
          const sql = 'delete from tbl_user_company where company_id = $[uuid] and user_id = $[user_id]'
    
          const res = await db.none(sql, payload)
  
          return res
        } catch (error) {
          console.log("🚀 ~ manageCompany > delete > error", error)
        }
      }
    }
  }
}

module.exports = new Client