const https = require('https')
const fs = require('fs')
const path = require('path')

const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')
const cors = require('cors')
const cookieParser = require('cookie-parser')

// server port
const PORT = 9000

const app = express()

// middleware
app.use(cors())
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())

// main routes
app.use('/api/auth', require('./route/api-auth'))
app.use('/api/company', require('./route/api-company'))
app.use('/api/employee', require('./route/api-employee'))
app.use('/api/doctor', require('./route/api-doctor'))
app.use('/api/examination', require('./route/api-examination'))
app.use('/api/audiometer', require('./route/api-audiometer'))
app.use('/api/filter', require('./route/api-filter'))
app.use('/api/report', require('./route/api-report'))
app.use('/api/analytic', require('./route/api-analytic'))

// SSL cert
// const sslPath = path.join(__dirname, './ssl')
// const key = fs.readFileSync(sslPath + '/server.key')
// const cert = fs.readFileSync(sslPath + '/server.cer')
// const ca = fs.readFileSync(sslPath + '/server_intermediate.cer')

app.listen(PORT, () => console.log(`app listening on port ${PORT}!`))