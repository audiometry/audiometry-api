const dao = require('../dao/dao-report')

const { Router } = require('express')
const router = Router()

// get report data
router.get('/', async (req, res) => {
  try {
    const data = await dao.manageReportQuery(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

module.exports = router