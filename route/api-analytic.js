const dao = require('../dao/dao-analytic')
const { validateRequest } = require('../utils/common')

const { Router } = require('express')
const router = Router()

// check if audiometer already exist
router.get('/examination', async (req, res) => {
  try {
    const data = await dao.fetchExamAnalytics(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

module.exports = router