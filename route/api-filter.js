const dao = require('../dao/dao-filter')

const { Router } = require('express')
const router = Router()

// get unique company options
router.get('/company', async (req, res) => {
  try {
    const data = await dao.fetchCompanyFilter(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})
router.get('/job', async (req, res) => {
  try {
    const data = await dao.fetchJobFilter(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})
router.get('/department', async (req, res) => {
  try {
    const data = await dao.fetchDepartmentFilter(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})
router.get('/section', async (req, res) => {
  try {
    const data = await dao.fetchSectionFilter(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})
router.get('/doctor', async (req, res) => {
  try {
    const data = await dao.fetchDoctorFilter(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})
router.get('/audiometer', async (req, res) => {
  try {
    const data = await dao.fetchAudiometerFilter(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})
module.exports = router