const dao = require('../dao/dao-doctor')

const { Router } = require('express')
const router = Router()


// check if doctor already exist
router.get('/check', async (req, res) => {
  try {
    const data = await dao.checkDoctorEntry(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

// add doctor
// update doctor
// delete doctor
router.route('/manage').post(async (req, res) => {
  try {
    const data = await dao.manageDoctor(req.body)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

// get doctor by user id
router.get('/fetch/user', async (req, res) => {
  try {
    const data = await dao.fetchByUser(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

module.exports = router