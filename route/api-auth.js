const { createUser, verifyUser, findUserById } = require('../dao/dao-user')

const { Router } = require('express')
const router = Router()

const jwt = require('jsonwebtoken')
const config = require('../data/auth-config')
const bcrypt = require('bcrypt')

// cookie last for 1 day
const maxAge = 1 * 24 * 60 * 60;

// // register
router.post('/register', async (req, res) => {
  const { name, email, password, phone, address } = req.body

  if (!name) {
    res.status(422).send({ error: 'You must provide a valid name' })
  }

  if (!email) {
    res.status(422).send({ error: 'You must provide a valid email' })
  }

  try {
    // hashing user password
    const salt = await bcrypt.genSalt()
    const hash = await bcrypt.hash(password, salt)

    // create user to db
    const user = await createUser({ name, email, password: hash, phone, address })

    if (user.uuid) {
      // create token
      const token = jwt.sign({ id: user.uuid }, config.secret, { expiresIn: maxAge })

      // attach cookie
      res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 })

      res.status(201).json({ user })
    } else {
      // somehow node doesnt catch error from pg here
      res.status(422).send({ error: user.detail })
    }
  } catch (error) {
    res.status(422).send({ error })
  }
})

// login
router.post('/login', async (req, res) => {
  const { email, password } = req.body

  if (!email) {
    res.status(422).send({ error: 'You must provide a valid email' })
  }

  if (!password) {
    res.status(422).send({ error: 'You must provide a valid password' })
  }

  try {
    // check user in db
    const user = await verifyUser(email)

    if (user) {
      const auth = await bcrypt.compare(password, user.password)

      if (auth) {
        // create token
        const token = jwt.sign({ id: user.uuid }, config.secret, { expiresIn: maxAge })

        // attach cookie
        res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 })

        res.status(200).json({ ...user })

      } else res.status(422).send({ error: 'Incorrect Password' })

    } else {
      res.status(422).send({ error: 'Incorrect Email' })
    }
  } catch (error) {
    res.status(422).send({ error })
  }
})

// logout
router.get('/logout', (req, res) => {
  try {
    res.cookie('jwt', '', { maxAge: 1 })
    return res.status(200).send({ status: 'OK' })
  } catch (error) {
    return res.status(500).send({ error })
  }
})

// me
router.get('/me', (req, res) => {
  const token = req.cookies.jwt

  // if any error : invalid token or invalid user
  // return null to force user to re-login

  if (token) {
    jwt.verify(token, config.secret, async (error, decodedToken) => {
      if (!error) {
        const user = await findUserById(decodedToken.id)

        if (user) return res.json({ ...user })
        else return res.json(null)
      } else return res.json(null)
    })
  } else return res.json(null)
})

module.exports = router