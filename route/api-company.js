const dao = require('../dao/dao-company')

const { Router } = require('express')
const router = Router()

// check if audiometer already exist
router.get('/check', async (req, res) => {
  try {
    const data = await dao.checkCompanyEntry(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

// add company
// update company
// delete company
router.route('/manage')
  .post(async (req, res) => {
    try {
      const data = await dao.manageCompany(req.body)
      res.status(200).json(data)
    } catch (err) {
      res.status(500).send({ error: err.toString() })
    }
  })

  // get companies by user id
router.get('/fetch/user', async (req, res) => {
  try {
    const data = await dao.fetchByUser(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

module.exports = router