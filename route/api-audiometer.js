const dao = require('../dao/dao-audiometer')
const { validateRequest } = require('../utils/common')

const { Router } = require('express')
const router = Router()

// check if audiometer already exist
router.get('/check', async (req, res) => {
  try {
    const data = await dao.checkAudiometerEntry(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

// add audiometer
// update audiometer
// delete audiometer
router.route('/manage')
  .post(async (req, res) => {
    try {
      const data = await dao.manageAudiometer(req.body)
      res.status(200).json(data)
    } catch (err) {
      res.status(500).send({ error: err.toString() })
    }
  })

// get audiometers by user id
router.get('/fetch/user', async (req, res) => {
  try {
    const data = await dao.fetchByUser(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

// get all audiometers
router.get('/fetch/all', async (req, res) => {
  try {
    const data = await dao.fetchByUser()
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

module.exports = router