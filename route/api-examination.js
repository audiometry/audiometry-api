const dao = require('../dao/dao-examination')
const { validateRequest } = require('../utils/common')

const { Router } = require('express')
const router = Router()

// check examination type
router.get('/check', async (req, res) => {
  try {
    const data = await dao.checkExamType(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

// check examination type
router.get('/company', async (req, res) => {
  try {
    const data = await dao.fetchCompanyID(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

// add examination
// update examination
// delete examination
router.route('/manage')
  .post(async(req, res) => {
    try {
      const data = await dao.manageExamination(req.body)
      res.status(200).json(data)
    } catch (err) {
      res.status(500).send({ error: err.toString() })
    }
  })

// get companies by examination
router.get('/fetch/user', async (req, res) => {
  try {
    const data = await dao.fetchByUser(req.query)
    res.status(200).json(data)
  } catch (err) {
    res.status(500).send({ error: err.toString() })
  }
})

module.exports = router