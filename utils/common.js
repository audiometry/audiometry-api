const self = module.exports = {
  validateRequest (required, req) {
    return new Promise((resolve, reject) => {
      const fields = req
      const { isSatisfied, missingKeys } = self.containKeys({ required, fields })
      if (!isSatisfied) {
        const err = new Error(`Required parameter:  ${missingKeys.join()}`)
        reject(err)
      } else {
        resolve(fields)
      }
    })
  },

  containKeys ({ required, fields }) {
    const queryKeys = Object.keys(fields).map(k => k.toString().toLowerCase())
    const isSatisfied = required.every(k => queryKeys.includes(k))
    const missingKeys = required.filter(k => !queryKeys.includes(k))
    return { isSatisfied, missingKeys }
  },

  pickObject (object, keys) {
    return keys.reduce((obj, key) => {
       if (object && object.hasOwnProperty(key)) {
          obj[key] = object[key];
       }
       return obj;
     }, {});
  }
}