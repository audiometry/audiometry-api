const { createUser } = require('../dao/dao-user')

const { Router } = require('express')
const router = Router()

const jwt = require('jwt-simple')
const config = require('../data/auth-config')
const bcrypt = require('bcrypt')


function getUserToken (user) {
  const timestamp = new Date().getTime()
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret)
}

function login (req, res, next) {
  const { password, ...rest } = req.user
  res.send({ token: getUserToken(req.user), ...rest })
}

function logout (req, res, next) {
  req.logout()
  res.send({})
}

function register (req, res, next) {
  const { name, email, password, phone, address } = req.body
  const saltRound = 12

  if (!email || !password) {
    res.status(422).send({ error: 'You must provide an email and password' })
  }

  bcrypt
    .hash(password, saltRound)
    .then(async (hash) => {
      return createUser({ name, email, password: hash, phone, address, type: '2' })
        .then((user) => {
          if (res.status === 200) {
            const { password, lm_date, ...rest } = user
            res.json({ token: getUserToken(res), ...rest })
          } else {
            return res.json({ ...user })
          }
        })
        .catch((err) => {
          console.log(err)
          return res.json({ error: 'Error creating user to database' })
        })
    })
    .catch((err) => {
      return next(err)
    })
}

module.exports = { login, logout, register }