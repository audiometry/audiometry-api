const pgpromise = require('pg-promise')

const conn = {
  user: 'postgres',
  password: '1234',
  host: 'localhost',
  port: '5432',
  database: 'audiometry'
}

const pgp = pgpromise({ schema: 'adm_v2' })
const db = pgp(conn)

module.exports = { pgp, db}